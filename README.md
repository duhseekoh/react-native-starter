# Overview
TODO

# TODO EPICS

- [x] Env config - Implement createEnvConfig.js
- [x] Logging - Setup logger
- [x] Style - Setup variables file
- [ ] Icons - Add guide for integration react-native-vector-icons
- [ ] Codepush - Add codepush
- [x] Flow - Add flow-typed
- [x] CI iOS - Create bitrise plan and test with ad-hoc and appstore build types
- [x] CI Android - Create bitrise plan and test with multiple build types
- [x] Apple - Get a developer account (setup first as an individual with Team Name Dom DiCicco)
- [ ] Apple - Get Pandera Labs DUNS number so we can get an organization account
- [ ] Auth0 - Add guide for integrating auth0
- [x] iOS - Add separate build configurations, by environment + app id
- [x] Android - Add separate build configuration, by environment + app id
- [ ] App Icon - Add a pandera labs icon for iOS and Android
- [ ] Tests - Add component tests using react-test-renderer
- [ ] Tests - Add example action creator test
- [ ] Tests - Add example reducer test
- [ ] Forms - Add redux-form
- [x] Redux - Module structure with examples
- [x] Component Structure - Routes with nested components
- [ ] Integrate prettier with eslint
- [x] Add example static image
- [ ] Add example custom font
- [ ] Add redux event tracking
- [ ] Look into storybook integration
- [ ] Add icons to react navigation tab bar

# Working on the Starter Kit
The starter kit itself is a project created from `react-native init`. Enhancements to the starter kit can be made just like you would with any other react native project.

# Using the Starter Kit
The first thing you'll want to do when using this project as a template for your own mobile app, is to rename `all the things`. It can be done in a few steps.

### Setup Apple Development Team
The starter kit uses xcode automatic code signing. You'll need to update the project team to your own (or use the included :TODO: Pandera Team if the project is staying in-house).

### Rename Project
1. Clone - Clone this repo to your new location. If you want to start with fresh git commit history, delete the `.git` directory.
1. Rename - Use `react-native-rename` to rename
1. iOS Bundle Ids - Open xcode -> ReactNativeStarter -> TARGETS -> ReactNativeStarter -> Build Settings -> Packaging -> Product Bundle Identifier -> Set bundle ids for each configuration (Debug, Release_Dev, Release)
1. iOS App names - Same screen as Bundle Ids, but go to the bottom and find BUNDLE_DISPLAY_NAME, rename to desired names.
1. Android App names - Open `android/app/build.gradle` and modify the `buildTypes` section.

# Development
TODO

# Environment configuration
Since you'll most likely be using this project to package up apps for multiple environments (e.g. dev, staging, prod) there is a prescribed approach built into this starter kit. This is a JS-only solution and avoids the need to setup variables how you typically would for native project.

### Setting up variables
Store your environment variables in a [dotenv](https://github.com/motdotla/dotenv) accessible file (e.g. .env). If you're in a CI build environment just use the environment variables feature available to you there.

### Making variables available
You'll setup what variables you would like available to the rest of your JS code in `tools/cli/createEnvConfig.js`. By running `yarn run createEnvConfig` a `envConfig.js` file will be created. This file exports a simple object with the environment variables defined inside. The final step to using these variables in your code is to put them inside your application configuration file located at `src/common/config`;

# Building
TODO

# Continuous Integration
Bitrise is used for building both installable apps for both iOS and Android. The CI setup is responsible for:
- Running tests
- Building iOS apps (.ipa) both ad-hoc and appstore
- Building Android apps (.apk)
- Uploading the latest jsbundle, by environment, to a codepush deployment
- Uploading JS sourcemaps to bugsnag

### Build Plans
iOS: https://www.bitrise.io/app/137dc02bff56060c
Android: TODO

### Workflows
Main workflows are:
- `build-dev-feature` - Builds the app for ad-hoc installs, *without* codepush
- `build-deploy-dev` - Builds the app for ad-hoc installs, *with* codepush
- `build-deploy-production` - Builds the app for appstore (or testflight) installs

While this setup should cover the needs of most projects, it is possible to add more workflows. For example, if your project needs staging builds that can be installed ad-hoc, then you could either configure `build-deploy-dev` to use an ad-hoc export method or add a new workflow that uses the ad-hoc export method. A third option may be to configure each of the main workflows to output both an ad-hoc and appstore build.

Note: Android follows the same workflows and codepush pattern, but does not differentiate between ad-hoc and appstore capable installations (because Android apks can be installed anywhere).

Note 2: Utility workflows also exist and are prefixed with an `_`. Utility workflows are combined to make up the main workflows.

### CI Environment variables
There are three utility workflows where environment variables should be specified. In fact, these three workflows don't do anything else, they are exclusive used to specify variables. Each are ran at the beginning of their corresponding main workflows.
- `_env-dev` - ran at start of `build-deploy-feature` and `build-deploy-dev`
- `_env-production`

# Directory Structure
TODO

# Navigation
This starter kit uses https://reactnavigation.org/. Highlights of React Navigation:
- Provides the two most common structures in mobile navigation, Stacks and Tabs.
- Has support for deep linking
- Provides typical transition animations
- Is the officially recommended navigation package for RN, and is actively being maintained

There is a `src/routes` directory which contains all possible screens one could route to. The `src/routes/Routes` file maintains configuration for all possible routes.

# API Communication
TODO

# Including Assets
React Native includes the ability to embed images, fonts, and videos directly into the packaged app. Here is where images and fonts are stored in this project and how they can be used.

### Images
Add png or jpeg files to `src/assets/images`. Include them as outlined in the RN docs: https://facebook.github.io/react-native/docs/image.html#image

### Fonts
Add fonts to `src/assets/fonts` and run `react-native link`. Now you can use the font name in a style declaration with `fontFamily: 'YourFontName'`.

### Icon sets
TODO - react-native-vector-icons

# Auth0 Integration
TODO

# Codepush
TODO

# Packages
Every package that is in package.json is listed with an explanation for inclusion here. Because everyone should know why each package is being pulled into the project.

* TODO
* TODO
* TODO
* TODO
* TODO
* etc...
