import 'prop-types';
import { AppRegistry } from 'react-native';
import App from './src/App';
AppRegistry.registerComponent('ReactNativeStarter', () => App);
