import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16.3';

// http://airbnb.io/enzyme/docs/installation/
Enzyme.configure({ adapter: new Adapter() });
