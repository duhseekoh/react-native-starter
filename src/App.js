// @flow
import React from "react";
import { AppState } from "react-native";
import { Provider, connect } from "react-redux";
import store from "src/redux/store";
import Routes from "src/routes/Routes";
import logger from 'src/common/logging/logger';

class App extends React.Component<*> {
  componentDidMount() {
    AppState.addEventListener("change", this.handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.handleAppStateChange);
  }

  handleAppStateChange(newAppState: "inactive" | "background" | "active") {
    if (newAppState === "inactive" || newAppState === "background") {
      logger.log("App sent to background");
    } else if (newAppState === "active") {
      logger.log("App brought to foreground");
    }
  }

  render() {
    return <Routes />;
  }
}

function mapStateToProps(/*state*/) {
  return {};
}

function mapDispatchToProps(/*dispatch*/) {
  return {
    // e.g. Add dispatch here if you need to call something upon app state change
  };
}

const ConnectedApp = connect(
  mapStateToProps,
  mapDispatchToProps
)(App);

const ConnectedProviderApp = () => (
  <Provider store={store}>
    <ConnectedApp />
  </Provider>
);

// TODO add codepush HOC wrapper here
// const CodePushedConnectedProviderApp = codePush({
//   deploymentKey: config.codepush.deploymentKey,
// })(ConnectedProviderApp);

export default ConnectedProviderApp;
