import envConfig from "../../envConfig";
import packageJson from "../package.json";

export default Object.freeze({
  envName: envConfig.ENV_NAME,
  // prefix to all of our endpoints
  apiBaseUrl: envConfig.API_BASE_URL,
  // bugsnag configuration
  // bugsnag: {
  //   apiKey: envConfig.BUGSNAG_API_KEY,
  //   stageName: envConfig.BUGSNAG_STAGE_NAME,
  // },
  // auth0 configuration, used for react-native-auth0
  // auth0: {
  //   clientId: envConfig.AUTH0_CLIENT_ID,
  //   domain: envConfig.AUTH0_DOMAIN,
  //   audience: envConfig.AUTH0_AUDIENCE,
  //   userClaimNamespace: envConfig.AUTH0_USER_CLAIM_NAMESPACE,
  //   dbConnectionName: envConfig.AUTH0_DB_CONNECTION_NAME,
  //   scope: 'openid email groups details roles permissions offline_access'
  // },
  // fcm: {
  //   senderId: envConfig.FCM_SENDER_ID,
  // },
  // codepush: {
  //   // unique based on ios vs android and environment deploying to
  //   deploymentKey: envConfig.CODEPUSH_DEPLOYMENT_KEY,
  // },
  displayEnvironmentDetails:
    envConfig.DISPLAY_ENVIRONMENT_DETAILS === "true" ? true : false,
  packageJson
});
