// @flow
/* eslint-disable no-console */
class Logger {
  // constructor() {
  //   // If you were using something like Bugsnag and/or loggly
  //   // then would instantiate those clients.
  // }

  log(message: string) {
    if (__DEV__) {
      console.log(message);
    }
  }

  // if desired, add more log levels as methods here
}

const logger = new Logger();

export default logger;
