import { Platform } from "react-native";

const colors = {
  black: "#000",
  red: "#F00",
  primaryText: "#666",
  translucentBG: "rgba(255,255,255,0.8)"
};

const fontSizes = {
  xlarge: 50,
  large: 20,
  medium: 14, // default font size (at least what react native has as default)
  small: 12,
  xsmall: 8
};

const fontFamilies = {
  base: "Helvetica",
  mono: Platform.OS === "ios" ? "Menlo" : "monospace"
};

const iconSizes = {
  xxxlarge: 50,
  xxlarge: 44,
  xlarge: 32,
  large: 24,
  medium: 18,
  small: 16,
  xsmall: 12
};

const textInputHeights = {
  medium: 40
};

export { colors, fontSizes, fontFamilies, iconSizes, textInputHeights };
