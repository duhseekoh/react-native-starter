// @flow
import * as React from "react";
import { Text, StyleSheet } from "react-native";
import { colors, fontSizes } from "src/common/style/variables";

type Props = {
  style?: any, // TODO fix
  children?: React.Node
};

function Txt(props: Props) {
  const { style, children } = props;
  return (
    <Text {...props} style={[styles.textStyle, style]}>
      {children}
    </Text>
  );
}

export default Txt;

const styles = StyleSheet.create({
  textStyle: {
    // Specify any default text styles here (font family, color, etc...)
    fontSize: fontSizes.medium,
    color: colors.primaryText
  }
});
