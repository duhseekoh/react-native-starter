import React from "react";
import Txt from "./Txt";
import { Text } from 'react-native';
import { shallow } from 'enzyme';

it("renders", () => {
  const wrapper = shallow(<Txt />);
  expect(wrapper).toMatchSnapshot();
});

it("passes props to Text", () => {
  const wrapper = shallow(<Txt one='1' two={2} />);
  const textCmps = wrapper.find(Text);
  expect(textCmps.length).toEqual(1);
  expect(textCmps.at(0).prop('one')).toEqual('1');
  expect(textCmps.at(0).prop('two')).toEqual(2);
});
