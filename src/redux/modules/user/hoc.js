// @flow
import { connect } from "react-redux";
import type { ReduxState } from "src/redux/rootReducer";
// import type { Dispatch } from "src/redux/reduxThunkTypes";
import {
  selectUserDetails,
  selectFavoriteZipcodes
} from "src/redux/modules/user/selectors";

type UserHOCConnectedStateProps = {
  details: {
    firstName: ?string,
    lastName: ?string
  },
  favoriteZipcodes: string[]
};

type UserHOCConnectedDispatchProps = {};

export type UserHOCProps = UserHOCConnectedStateProps &
  UserHOCConnectedDispatchProps &
  *;

function mapStateToProps(state: ReduxState): UserHOCConnectedStateProps {
  return {
    details: selectUserDetails(state),
    favoriteZipcodes: selectFavoriteZipcodes(state)
  };
}

function mapDispatchToProps(): UserHOCConnectedDispatchProps {
  /*dispatch: Dispatch*/
  return {
    // e.g. Dispatch actions that will fetch something from the network
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
);
