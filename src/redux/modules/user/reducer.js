// @flow
import type { ReduxStateUser, UserAction } from "./types";

const initialState: ReduxStateUser = {
  firstName: "Pete",
  lastName: "Larney",
  favoriteZipcodes: ["60622", "60601", "10001"]
};

function user(state: ReduxStateUser = initialState, action: UserAction) {
  switch (action.type) {
    default:
      return {
        ...state
      };
  }
}

export default user;
