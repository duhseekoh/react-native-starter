// @flow
import type { ReduxState } from "../../rootReducer";

/**
 * Select the zipcodes the user has chosen as favorites.
 */
export const selectFavoriteZipcodes = (state: ReduxState): * =>
  state.user.favoriteZipcodes;

/**
 * Select attributes of the user
 */
export const selectUserDetails = (state: ReduxState): * => ({
  firstName: state.user.firstName,
  lastName: state.user.lastName
});
