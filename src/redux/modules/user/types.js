// @flow
export type ReduxStateUser = {
  firstName: ?string,
  lastName: ?string,
  favoriteZipcodes: string[]
};

// See weather types for Action examples
export type UserAction = any;
