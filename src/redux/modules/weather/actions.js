// @flow
import type {
  ForecastFetchStartedAction,
  ForecastFetchSuccessAction,
  Forecast
} from "./types";
import type { Dispatch } from "../../reduxThunkTypes";

export const FORECAST_FETCH_STARTED = "FORECAST_FETCH_STARTED";
export function forecastFetchStarted(
  zipcode: string
): ForecastFetchStartedAction {
  return {
    type: FORECAST_FETCH_STARTED,
    payload: zipcode
  };
}

export const FORECAST_FETCH_SUCCESS = "FORECAST_FETCH_SUCCESS";
export function forecastFetchSuccess(
  forecast: Forecast
): ForecastFetchSuccessAction {
  return {
    type: FORECAST_FETCH_SUCCESS,
    payload: forecast
  };
}

export function getForecast(zipcode: string) {
  return async (dispatch: Dispatch /*, getState: Function*/): * => {
    // fake an http call
    dispatch(forecastFetchStarted(zipcode));
    const requestPromise: Promise<Forecast> = new Promise(resolve => {
      setTimeout(() => {
        if (zipcode === "60622") {
          resolve({
            zipcode: "60622",
            name: "Wicker Park, Chicago",
            temp: 85,
            wind: "10mph NW"
          });
        } else if (zipcode === "60601") {
          resolve({
            zipcode: "60601",
            name: "The Loop, Chicago",
            temp: 83,
            wind: "15mph NW"
          });
        } else {
          resolve({
            zipcode: "10001",
            name: "New York City",
            temp: 74,
            wind: "8mph SE"
          });
        }
      }, 1500);
    });
    const forecast: Forecast = await requestPromise;
    dispatch(forecastFetchSuccess(forecast));
    return forecast;
  };
}
