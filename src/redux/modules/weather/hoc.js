// @flow
import { connect } from "react-redux";
import type moment from "moment";
import type { ReduxState } from "src/redux/rootReducer";
import type { Dispatch } from "src/redux/reduxThunkTypes";
import { getForecast } from "src/redux/modules/weather/actions";
import type { Forecast } from "src/redux/modules/weather/types";
import {
  selectForecast,
  selectForecastLastUpdated,
  selectIsForecastLoading,
  selectRelatedZipcodes
} from "src/redux/modules/weather/selectors";

type WeatherHOCOwnProps = {
  zipcode: string
};

type WeatherHOCConnectedStateProps = {
  isLoading: boolean,
  lastUpdated: ?moment,
  forecast: ?Forecast,
  relatedZipcodes: string[]
};

type WeatherHOCConnectedDispatchProps = {
  getForecast: string => Promise<*>
};

export type WeatherHOCProps = WeatherHOCOwnProps &
  WeatherHOCConnectedStateProps &
  WeatherHOCConnectedDispatchProps;

function mapStateToProps(
  state: ReduxState,
  ownProps: WeatherHOCOwnProps
): WeatherHOCConnectedStateProps {
  return {
    isLoading: selectIsForecastLoading(state, ownProps.zipcode),
    lastUpdated: selectForecastLastUpdated(state, ownProps.zipcode),
    forecast: selectForecast(state, ownProps.zipcode),
    relatedZipcodes: selectRelatedZipcodes(state, ownProps.zipcode)
  };
}

function mapDispatchToProps(
  dispatch: Dispatch
): WeatherHOCConnectedDispatchProps {
  return {
    // e.g. Dispatch actions that will fetch something from the network
    getForecast: (zipcode: string) => dispatch(getForecast(zipcode))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
);
