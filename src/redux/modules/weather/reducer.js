// @flow
import moment from "moment";
import { FORECAST_FETCH_SUCCESS, FORECAST_FETCH_STARTED } from "./actions";
import type { ReduxStateWeather, WeatherAction } from "./types";

const initialState: ReduxStateWeather = {
  forecastsByZipcode: {},
  forecastRequestsByZipcode: {}
};

function weather(
  state: ReduxStateWeather = initialState,
  action: WeatherAction
): ReduxStateWeather {
  switch (action.type) {
    case FORECAST_FETCH_STARTED: {
      const zipcode = action.payload;
      return {
        ...state,
        forecastRequestsByZipcode: {
          ...state.forecastRequestsByZipcode,
          [zipcode]: { status: "IN_PROGRESS" }
        }
      };
    }
    case FORECAST_FETCH_SUCCESS: {
      const forecast = action.payload;
      return {
        ...state,
        forecastsByZipcode: {
          ...state.forecastsByZipcode,
          [forecast.zipcode]: forecast
        },
        forecastRequestsByZipcode: {
          ...state.forecastRequestsByZipcode,
          [forecast.zipcode]: {
            status: "SUCCESS",
            timestamp: moment().valueOf()
          }
        }
      };
    }
    default:
      return {
        ...state
      };
  }
}

export default weather;
