// @flow
import moment from "moment";
import type { ReduxState } from "../../rootReducer";
import type { Forecast } from "./types";

const selectForecastsByZipcode = (state: ReduxState): * =>
  state.weather.forecastsByZipcode;
const selectForecastRequestsByZipcode = (state: ReduxState): * =>
  state.weather.forecastRequestsByZipcode;

/**
 * Select an individual forecast request.
 * For containers curious about the state of a particular forecast request.
 */
const selectForecastRequest = (state: ReduxState, zipcode: string): * => {
  return selectForecastRequestsByZipcode(state)[zipcode];
};

/**
 * Select an individual forecast.
 * For containers displaying a single forecast.
 */
export const selectForecast = (
  state: ReduxState,
  zipcode: string
): ?Forecast => {
  return selectForecastsByZipcode(state)[zipcode];
};

/**
 * Determine if an individual forecast request is in flight
 */
export const selectIsForecastLoading = (
  state: ReduxState,
  zipcode: string
): boolean => {
  const request = selectForecastRequest(state, zipcode);
  return request ? request.status === "IN_PROGRESS" : false;
};

/**
 * Get the last time a forecast was updated
 */
export const selectForecastLastUpdated = (
  state: ReduxState,
  zipcode: string
): ?moment => {
  const request = selectForecastRequest(state, zipcode);
  return request && request.timestamp ? moment(request.timestamp) : undefined;
};

export const selectRelatedZipcodes = (
  state: ReduxState,
  zipcode: string
): string[] => {
  if (zipcode) {
    return ["60601", "60622"];
  }
  return [];
};
