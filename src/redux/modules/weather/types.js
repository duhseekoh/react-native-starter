// @flow
export type Forecast = {
  zipcode: string,
  name: string,
  temp: number,
  wind: string
};

export type ReduxStateWeather = {
  forecastsByZipcode: {
    [zipcode: string]: Forecast
  },
  forecastRequestsByZipcode: {
    [zipcode: string]: {
      status: "SUCCESS" | "IN_PROGRESS" | "FAILURE",
      timestamp?: number
    }
  }
};

export type WeatherAction =
  | ForecastFetchStartedAction
  | ForecastFetchSuccessAction;

export type ForecastFetchStartedAction = {
  type: "FORECAST_FETCH_STARTED",
  payload: string
};

export type ForecastFetchSuccessAction = {
  type: "FORECAST_FETCH_SUCCESS",
  payload: Forecast
};
