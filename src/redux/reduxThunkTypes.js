// @flow
import type { ReduxState } from "./rootReducer";

export type Action = $Shape<{
  type: string,
  payload?: any,
  meta?: ?Object
}>;
export type Dispatch = (action: Action | ThunkAction | PromiseAction) => any; // eslint-disable-line
export type GetState = () => ReduxState;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;
