// @flow
import { combineReducers } from "redux";

import user from "./modules/user/reducer";
import type { ReduxStateUser } from "./modules/user/types";
import weather from "./modules/weather/reducer";
import type { ReduxStateWeather } from "./modules/weather/types";

export type ReduxState = {
  user: ReduxStateUser,
  weather: ReduxStateWeather
};

const reducers = combineReducers({
  user,
  weather
});

export default reducers;
