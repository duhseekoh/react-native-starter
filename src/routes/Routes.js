// @flow
import {
  createStackNavigator,
  createBottomTabNavigator,
  type NavigationStackScreenOptions
} from "react-navigation";
import HomeScreen from "./home/HomeScreen";
import WeatherListScreen from "./weather-list/WeatherListScreen";
import WeatherDetailScreen from "./weather-detail/WeatherDetailScreen";

/**
 * All screens get this default configuration. It's mainly to style the header
 * of each screen consistently. Overrides can be given at the screen level.
 */
const defaultStackOptions: NavigationStackScreenOptions = {
  headerBackTitle: " ", // Turns off back button text
  headerTitleStyle: {
    fontSize: 24
  }
};

// Routes that don't fit under one tab.
// e.g. you might access Weather detail from both the home tab and locations tab.
const UniversalRoutes = {
  WeatherDetail: {
    screen: WeatherDetailScreen,
    path: "/weatherdetail/:zipcode" // zipcode is passed in as a state parameter
  }
};

// Each tab gets its own stacknavigator
const HomeTab = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      path: "/home" // TODO these will be used when we hook up universal linking
    },
    ...UniversalRoutes
  },
  {
    navigationOptions: defaultStackOptions
  }
);

const WeatherTab = createStackNavigator(
  {
    WeatherList: {
      screen: WeatherListScreen,
      path: "/weatherlist"
    },
    ...UniversalRoutes
  },
  {
    navigationOptions: defaultStackOptions
  }
);

const Routes = createBottomTabNavigator(
  {
    HomeTab: {
      screen: HomeTab,
      path: "/hometab",
      navigationOptions: {
        // If this is not specified, the title of the active screen within this tab
        // will be displayed as the tab bar label.
        tabBarLabel: "Home"
      }
    },
    WeatherTab: {
      screen: WeatherTab,
      path: "/weathertab",
      navigationOptions: {
        tabBarLabel: "Weather"
      }
    }
  },
  {
    initialRouteName: "HomeTab"
  }
);

export default Routes;
