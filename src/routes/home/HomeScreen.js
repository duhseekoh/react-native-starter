// @flow
import React, { Component } from "react";
import { View, ImageBackground } from "react-native";
import type {
  NavigationStackScreenOptions,
  NavigationNavigatorProps
} from "react-navigation";
import UserSummary from "./components/user-summary/UserSummary";

type NavigationStateParams = *;
type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class HomeScreen extends Component<NavigationProps> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Home Screen"
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          style={{ flex: 1 }}
          imageStyle={{ opacity: .1 }}
          resizeMode='repeat'
          source={require("src/assets/images/pandera-logo.png")}
        >
          <UserSummary firstName="Peter" lastName="Larney" />
        </ImageBackground>
      </View>
    );
  }
}

export default HomeScreen;
