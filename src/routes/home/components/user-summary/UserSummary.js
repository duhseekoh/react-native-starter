// @flow
import React from "react";
import { View } from "react-native";
import Txt from "src/components/text/Txt";

type Props = {
  firstName: string,
  lastName: string
};

export default function UserSummary({ firstName, lastName }: Props) {
  return (
    <View>
      <Txt>
        Welcome {firstName} {lastName}
      </Txt>
    </View>
  );
}
