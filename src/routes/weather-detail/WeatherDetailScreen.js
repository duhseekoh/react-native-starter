// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  type NavigationEventSubscription,
  type NavigationNavigatorProps,
  type NavigationStackScreenOptions
} from "react-navigation";
import WeatherDetail from "./components/weather-detail/WeatherDetail";
import HeaderRightRefresh from "./components/header-right-refresh/HeaderRightRefresh";
import type { ReduxState } from "src/redux/rootReducer";
import type { Dispatch } from "src/redux/reduxThunkTypes";
import { getForecast } from "src/redux/modules/weather/actions";
import { selectIsForecastLoading } from "src/redux/modules/weather/selectors";

type NavigationStateParams = {
  params: {
    zipcode: string
  }
} & *;

type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

type ConnectedStateProps = {
  isLoading: boolean
};

type ConnectedDispatchProps = {
  getForecast: string => any
};

type Props = NavigationProps & ConnectedStateProps & ConnectedDispatchProps;

function mapStateToProps(
  state: ReduxState,
  ownProps: NavigationProps
): ConnectedStateProps {
  const zipcode = ownProps.navigation.state.params.zipcode;
  return {
    isLoading: selectIsForecastLoading(state, zipcode)
  };
}

function mapDispatchToProps(dispatch: Dispatch): ConnectedDispatchProps {
  return {
    // e.g. Dispatch actions that will fetch something from the network
    getForecast: (zipcode: string) => dispatch(getForecast(zipcode))
  };
}

class WeatherDetailScreen extends Component<Props> {
  _navigationListener: NavigationEventSubscription;
  static navigationOptions = ({
    navigation
  }: *): NavigationStackScreenOptions => {
    return {
      title: `Weather for ${navigation.state.params.zipcode}`,
      headerRight: (
        <HeaderRightRefresh zipcode={navigation.state.params.zipcode} />
      )
    };
  };

  componentDidMount() {
    this._navigationListener = this.props.navigation.addListener(
      "didFocus",
      () => {
        const { getForecast } = this.props;
        const { zipcode } = this.props.navigation.state.params;
        // initial request for the forecast and any other time this screen gets focused
        getForecast(zipcode);
      }
    );
  }

  componentWillUnmount() {
    this._navigationListener.remove();
  }

  render() {
    const zipcode = this.props.navigation.state.params.zipcode;
    return <WeatherDetail zipcode={zipcode} />;
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WeatherDetailScreen);
