// @flow
import React from "react";
import { TouchableOpacity } from "react-native";
import Txt from "src/components/text/Txt";
import withWeather, {
  type WeatherHOCProps
} from "src/redux/modules/weather/hoc";

function HeaderRightRefresh(props: WeatherHOCProps) {
  const { getForecast, zipcode, isLoading } = props;
  return (
    <TouchableOpacity onPress={() => getForecast(zipcode)}>
      <Txt>{isLoading ? "fetching..." : "Refresh"}</Txt>
    </TouchableOpacity>
  );
}

export default withWeather(HeaderRightRefresh);
