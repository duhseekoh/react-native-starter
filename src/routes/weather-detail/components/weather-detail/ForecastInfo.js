// @flow
import React from "react";
import moment from "moment";
import { View, StyleSheet } from "react-native";
import Txt from "src/components/text/Txt";
import type { Forecast } from "src/redux/modules/weather/types";
import { fontSizes } from "src/common/style/variables";

type Props = {
  style?: any, // TODO fix
  forecast: Forecast,
  lastUpdated: ?moment
};

const ForecastInfo = (props: Props) => {
  return (
    <View style={[styles.container, props.style]}>
      <Txt style={styles.locationText}>{props.forecast.name}</Txt>
      <Txt style={styles.zipText}>({props.forecast.zipcode})</Txt>
      <Txt style={styles.tempText}>High {props.forecast.temp}</Txt>
      <Txt style={styles.windText}>Wind {props.forecast.wind}</Txt>
      {props.lastUpdated && (
        <Txt style={styles.lastUpdatedText}>
          Last Updated {props.lastUpdated.format("LT")}
        </Txt>
      )}
    </View>
  );
};

export default ForecastInfo;

const styles = StyleSheet.create({
  container: {
    margin: 20
  },
  locationText: {
    fontSize: fontSizes.large,
    fontWeight: "bold"
  },
  zipText: {
    fontSize: fontSizes.small
  },
  tempText: {
    fontSize: fontSizes.large
  },
  windText: {
    fontSize: fontSizes.small
  },
  lastUpdatedText: {
    fontSize: fontSizes.small,
    fontStyle: "italic"
  }
});
