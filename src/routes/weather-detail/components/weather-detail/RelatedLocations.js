// @flow
import React from "react";
import { View, TouchableOpacity } from "react-native";
import Txt from "src/components/text/Txt";

const RelatedLocations = (props: {
  zipcodes: string[],
  onSelectLocation: (zipcode: string) => *
}) => {
  return (
    <View>
      <Txt>Related Locations</Txt>
      {props.zipcodes.map(zipcode => (
        <TouchableOpacity
          key={zipcode}
          onPress={props.onSelectLocation.bind(this, zipcode)}
        >
          <Txt>{zipcode}</Txt>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default RelatedLocations;
