// @flow
import React, { Component } from "react";
import { View } from "react-native";
import {
  withNavigation,
  NavigationActions,
  type NavigationNavigatorProps
} from "react-navigation";
import Txt from "src/components/text/Txt";
import withWeather, {
  type WeatherHOCProps
} from "src/redux/modules/weather/hoc";
import ForecastInfo from "./ForecastInfo";
import RelatedLocations from "./RelatedLocations";

class WeatherDetail extends Component<
  WeatherHOCProps & NavigationNavigatorProps<*, *>
> {
  componentDidMount() {
    const { getForecast, zipcode, forecast, isLoading } = this.props;
    if (!forecast && !isLoading) {
      getForecast(zipcode);
    }
  }

  render() {
    const { forecast, lastUpdated, relatedZipcodes, isLoading } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <Txt>
          {isLoading && !forecast && "Getting your updated forecast..."}
        </Txt>
        {forecast && (
          <ForecastInfo forecast={forecast} lastUpdated={lastUpdated} />
        )}
        <RelatedLocations
          zipcodes={relatedZipcodes}
          onSelectLocation={selectedZipcode => {
            this.props.navigation.dispatch(
              NavigationActions.navigate({
                routeName: "WeatherDetail",
                params: { zipcode: selectedZipcode },
                key: `weather-detail-${selectedZipcode}`
              })
            );
          }}
        />
      </View>
    );
  }
}

export default withNavigation(withWeather(WeatherDetail));
