// @flow
import React, { Component } from "react";
import { View } from "react-native";
import type {
  NavigationStackScreenOptions,
  NavigationNavigatorProps
} from "react-navigation";
import { NavigationActions } from "react-navigation";
import logger from 'src/common/logging/logger';
import WeatherList from "./components/weather-list/WeatherList";

type NavigationStateParams = *;
type NavigationProps = NavigationNavigatorProps<*, NavigationStateParams>;

class WeatherListScreen extends Component<NavigationProps> {
  static navigationOptions: NavigationStackScreenOptions = {
    title: "Locations"
  };

  componentDidMount() {
    logger.log('WeatherListScreen mounted');
  }

  handleSelectLocation = (zipcode: string) => {
    const { dispatch } = this.props.navigation;

    dispatch(
      NavigationActions.navigate({
        routeName: "WeatherDetail",
        params: { zipcode }
      })
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <WeatherList
          onSelectLocation={this.handleSelectLocation}
          // TODO - connect to 'user' redux module
          zipcodes={["60622", "60601", "10001"]}
        />
      </View>
    );
  }
}

export default WeatherListScreen;
