// @flow
import React from "react";
import { TouchableOpacity, StyleSheet } from "react-native";
import Txt from "src/components/text/Txt";

type Props = {
  style?: any, // TODO Fix
  zipcode: string,
  onPress: (*) => any
};

const ListItem = ({ zipcode, onPress, style }: Props) => (
  <TouchableOpacity onPress={onPress} style={style}>
    <Txt style={styles.itemText}>{zipcode}</Txt>
  </TouchableOpacity>
);

ListItem.defaultProps = {
  onPress: () => {}
};

export default ListItem;

const styles = StyleSheet.create({
  itemText: {
    fontSize: 42
  }
});
