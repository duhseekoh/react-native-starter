// @flow
import React from "react";
import { View } from "react-native";
import ListItem from "./ListItem";

type Props = {|
  /*define props passed in through a parent component here*/
  zipcodes: string[],
  onSelectLocation: (zipcode: string) => any
|};

class WeatherList extends React.Component<Props> {
  render() {
    const { zipcodes, onSelectLocation } = this.props;

    return (
      <View>
        {zipcodes.map(zipcode => (
          <ListItem
            key={zipcode}
            zipcode={zipcode}
            onPress={onSelectLocation.bind(this, zipcode)}
          />
        ))}
      </View>
    );
  }
}

export default WeatherList;
