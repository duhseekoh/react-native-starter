// This should be run during local environment setup and at bundle time to
// generate the environment configuration file that will be packaged up into the js bundle.
const fs = require('fs');
require('dotenv').config();

function getOrThrow(envVar, name) {
  if (!envVar) {
    throw new Error(`Required environment variable ${name} not specified`);
  }

  return envVar;
}

const config = {
  ENV_NAME: getOrThrow(process.env.ENV_NAME, 'ENV_NAME'),
  DISPLAY_ENVIRONMENT_DETAILS: process.env.DISPLAY_ENVIRONMENT_DETAILS,
  /*Add more environment variables here*/
};

try {
  fs.writeFileSync('envConfig.js', `export default ${JSON.stringify(config)}`);
} catch (e) {
  console.log('Could not create environment configuration file ', e);
  throw (e);
}
